# Finding your zen in 16.0

`navigation` `productivity` 

A major theme for us in version 15 was listening to what you all have told us – finding your way around GitLab is _hard_. We take this to heart and want GitLab to be as approachable as any other application in your toolbelt. As GitLab has grown so has it's array of rich features which consequently has created clutter and noise in the UI. Fundamentally the scafolding that was in place for navigating around was functional, but what we were left with was hindering users. Here are some highlights we think you'll love.

## Making it all about your work

Making you productive is what ultimately matters. You should be able to pickup where you left off without having to keep 35+ tabs open.

- **Drafts**. Ever start a comment and forget about? Us to. The drafts page will keep track of your work so you don't have to. Just use the search bar to find it when you need to.
- **Recently visited**. Yes your browser knows your web history but it's cluttered by all the other sites you also visit throughout your day.
- **Merge request pipelines**. If the pipelines are done, then it's time to take action. No more keeping a tab open to watch that status indiciator change. There's now a pipeline status indicator directly in the top right of the navigation for the merge requests you've added a commit to. You'll get a nice toast notification when it's all done, and a record in your notification drawer.
- **Favorites**. You can now add any page in GitLab. No more relying on browser bookmarks or hacks to keep track of important items.
- **Pinned projects**. There are thousands of projects in GitLab.com. We know because we don't let our badge counter grow beyond 1000+, but we've learned that most only care about 3-5 group or projects at a time. You can now pin projects that you've starred for quick access. No more clicking through multiple drodowns and tabs to find Billy's project.

## All new homepage

The perfect launch point. Did you know you could change what was previously on the home page of GitLab? Most didn't so we made this space to cater to you. It'll help you onboard or keep up with your colleagues, while also allowing you to switch tasks without having to check your email. We really don't like emails.

## A proper notification center and task tracker

No more 99+ To-Do's. There'll be a dot indicator that appears on the notification bell when something needs your attention. We've revamped this area of the product to bring to your attention what you haven't seen. 

## Simplifying the left siderbar

At some point, the left sidebar for projects had grown to over 12 parent items and most of them were not relevant to a current workflow. We've cut that down to no more than 5. To do this, we had to more closely integrate features into their workflow and great example of the is the WebIDE. You can access this at anytime in a project by pressing <kbd>.</kbd> or by visting the repository page. There's no need to have this take precious space and cognitive load when it's not needed.

- **For administrators**: We split pages between Overview, Hardware, Configuration, and Preferences.
- **For groups**. It's all about managing your team and knowing how things are progressing. 
- **For projects**. Maybe you care about code, or maybe you don't. We assume each project will carry out the DevOps lifecycle, but owners can hide what they don't need from view. To simplify things we've distilled our pages into Overview, Build, Deploy, and Secure. We took that list of 64 pages and brought it down to 20 rich features. We cut some links and moved around some others to keep you focused on the task at hand - making and growing the project you care about.

We recommend some things, but you can customize your needs. GitLab is meant for anyone but it's hard to tailor it for everyone. You can choose to add what you prefer in your left sidebar categories.

## Keyboard users rejoice

We've lovingly tested the keyboard interactions and helped highlight where shortcuts can help you move around faster. Your keyboard shouldn't be what slows you down when navigating you around GitLab.

## Fresh coat of paint

We had lots of little details that may not stand out to you at first, but they are there. Spacing has been cleaned up, breakpoints respond more seamlessly, there is less reliance on color for differentiation, and less is more now.

## One last thing... Try searching

Trust us. You might find your new favorite shortcut just one-click away or that setting you forgot how to get to can be easily discoverd.

---

We try and do everything we can inside GitLab. We live with the same experience that you do, and acknowledge that every person works a bit differently so there's a little bit for everyone.
