## A personal retrospective

> **Disclaimer:** I'm first writing this when I am tired from opening numerous issues to coordinate effort.

As far as I can tell, [Category Maturity Scorecards (CMS)](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/) are a GitLab defined and implemented user research mechanism. The outcome should be to validate a rating for category which we proudly display on our [maturity page](https://about.gitlab.com/direction/maturity/). I assume before CMS was implemented, individual maturity levels were driven by gut feel and quantitative metrics. These same expecations exist, however, a quantitative rating was introduced and has brought with it some unintended consequences.

- Designers have to carve time away from other validation and build track work to discover this score
- Designers & researchers expend significant effort across numerous milestones to setup, coordinate, dry-run, recruit, test, synthesize, and publish outcomes
- As a result, the feedback loop for knowing a CMS takes too long to bring forth
- A tension has been created with Product Management, who is trying to define the rough timeline of when a category will reach a new maturity level
- Currently, roadmaps (built in epics) outline features, that are expected to be necessary in order to reach a new level of maturity, but don't actually validate the score themselves
- For broader reaching categories, it seems quite impossible to do a comprehensive test that is repeatable and not subjective 


**So what do I think could be done?**

> Scoring sessions conducted synchronously should be avoided

**Consider:** Scoring should be based on both quantative and qualitative feedback, but is should be able to be captured on a quarterly basis
  - Make it simple enough that it requires little overhead to get the feedback
  - Tie categories to system usability scale like questions and segment based on personas
  - Use metrics that are captured in Sisense to determine usage trends and projections

> UX Research should become the DRI responsiblity for CMS (if it isn't already)

**Why?** For product design, it is too detracting for other efforts related to [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/)

> Use the CMS process only as a way to populate future opportunities for the roadmap

**Current problem:** without the score a maturity rating will not change even the other expectations are met

---

To be continued...
