## Audit events

### Jobs to be done: When I have complete visibility into adherence of policies, I want to monitor the condition of our software development practices, so that I can address issues before they become more problematic.

#### Task A  - Monitor

> Prompt: In this scenario you are responsible for monitoring any events that might pertinent in an upcoming audit. 

**Scenario 1:  What are 5 events that you would deem critical that GitLab capture and be discoverable for you? An example might be a user being added or removed.**

Success: All of the events that participants share would be captured in our list of events.

**Scenario 2:  You’ll need to discover if anyone users were able to approve their own merge requests in project XYZ. How would you do that?** 

Success: Participants should identify the specific events in the Audit Events project page that provide an opportunity for a user to approve their own merge request.

### Jobs to be done: When I am preparing for an audit, I want to create shareable deliverables, so that I can provide evidence of compliance.

#### Task B  - Create

> Prompt: You have an upcoming audit in two weeks. To prepare for it ahead of time, you want to create a few shareable deliverables with your auditor.

**Scenario 3: Create a shareable file that shows only access changes to project XYZ.**

Success: Participants should be able to export the audit events to a .csv file, and then modify it in such a way that it only shows access records.

**Scenario 4: Imagine you have a server to stores all your data on premise. How would you send your GitLab event data there?**

Success: Participants should at least be able to identify the steps in docs.

## Compliance Management

### Jobs to be done: When restrictions are necessary, I want to configure an environment, so that I can ensure we are compliant.

#### Task A  - Configuring separation of duties

> Prompt: In GitLab, you need to ensure that your organization, Group X, has been properly configured so that its projects have adequate separation of duties. I’ll be providing you with scenarios to complete one-by-one over chat. 

**Scenario 1: Only maintainers should be able to push to the default branch, and approval from codeowners should also be required.** 

Success: Only project Alpha must have its default branch protection changed. Beta will already be properly configured.
- Allowed to push: Developers + Maintainers → Maintainers. 
- FYI: Others projects will already be set to Maintainers  and codeowners approval will be enabled

**Scenario 2: Every future merge request for the default branch should require at least 2 approvers**

Success: Either of these two options
- Add an approval rule to each project for main with 2 approvers
- Make the “all eligible approvers” set to 2

**Scenario 3: Projects Alpha and Beta should not allow authors to merge their own code changes. More specifically, the maintainers of these projects should not be able to change this setting.**

Success: Group X should have it’s merge request approval setting for “Prevent approval by author.” enabled to lock down the setting in Alpha and Beta.

**Scenario 4: CI/CD configuration for projects in Group X should point to the CI file set in Project Gamma**

Success: Set the General pipelines > CI/CD configuration file path to gitlab-ci.yml@group-y/gamma in project’s Alpha & Beta.

#### Task B - Creating a safe environment with required defaults

> Prompt: Next, you’ll need to implement a handful of controls in GitLab that create safeguards from risk.

**Scenario 5: Restrict users from requesting access to projects in Group X.**

Success: Uncheck group permissions to allow users to request access in Group X, Alpha and Beta

**Scenario 6: No projects should be exposed to the public.**

Success: Change project Alpha to internal; Beta will already be internal

**Scenario 7: Project Alpha is subject to SOX compliance, and should be uniquely differentiated from other projects.**

Success: Both steps must be completed in sequential order
- Create a compliance framework in Group X
- Assign the framework to projects Alpha

**Scenario 8: Every code change for Project Alpha needs to pass a validation check with https://gitlab-bot.service-api.com**

Success: Add an external status check for that address in the project under Settings > General > Merge Request > Status Checks

**Scenario 9: The default branch must be fully protected for all future projects in Group X.**

Success: Set default branch protection to `Fully protected…`

### Job to be done: When I have complete visibility into adherence of policies, I want to monitor the condition of our software development practices, so that I can address issues before they become more problematic.

#### Task C  - Monitoring for compliance issues

> Prompt: Some time has passed, and you’ll need to check for any violations of your controls that might have occurred. I’ll be providing you with tasks to verify these controls over chat.

**Scenario 10: Identify which merge requests had a separation of duties violation. Are there any violations? If so what are they.**

Success: Checks the compliance dashboard for merge requests that meet this criteria
- Violation: Both Alpha and Beta have a merge request that was merged by their author in production. 

**Scenario 11: Users should only have inherited their access from their group, meaning there should be no users granted access directly from their project. Have any users been added directly to a project?**

Success: Checks project membership to view how users got access or uses group membership export feature to check.

<details><summary>Environment setup (just for Austin)</summary>

The JTBD will focus on scenarios that can be executed in SaaS, and not the exclusive self-managed features.

https://amaranth-falcon-b2eyoug1.ws-us17.gitpod.io/

1. Launch GitPod from the master branch of the GitLab project
1. Set root user password to ...
1. Add an Ultimate License
    1. `Admin Area > Subscriptions`
1. Modify GitLab Org name to be Group X 
    1. `Group > Settings > General / Naming, visibility`
1. Modify GitLab Org path to be group-x 
    1. `Group > Settings > General / Advanced - Change group URL`
1. Modify GitLab Test to be Alpha
    1. `Project > Settings > General / Naming, topics, avatar`
1. Modify GitLab Test path to be alpha
    1. `Project > Settings > General / Advanced - Change path`
1. Modify GitLab Shell to be Beta
    1. `Project > Settings > General / Naming, topics, avatar`
1. Modify GitLab Shell path to be beta
    1. `Project > Settings > General / Advanced - Change path`
1. Modify Commit451 name to be Group Y 
    1. `Group > Settings > General / Naming, visibility`
1. Modify Commit451 path to be group-y 
    1. `Group > Settings > General / Advanced - Change group URL`
1. Modify Lab Coat to be Gamma
    1. `Project > Settings > General / Naming, topics, avatar`
1. Modify Lab Coat path to be gamma
    1. `Project > Settings > General / Advanced - Change path`
1. Add Denver Reinger, @reported_user_19 as an Owner to Group X & Y
    1. `Group information > Members`
1. Edit Denver Reinger to be using the password ...
    1. `Admin Area > Users > Edit`
1. Impersonate Denver Reinger and dismiss alerts
    1. `Customize this page banner`
    1. SSH Key warning when viewing any other page (Don’t show again)
    1. Auto DevOps for Alpha, Beta, and Gamma
1. Sign out of root and sign in  as reported_user_19 set the password as ... again
1. Check the compliance report for activity in Group X
    1. `Group X > Security & Compliance > Compliance report`
1. Add a code owners file to each projects in Group X 
    1. `alpha / .gitlab / CODEOWNERS`
        1. `README.md @group-x
    1. `beta / .gitlab / CODEOWNERS` (Already should exist)
        1. `* @ashmckenzie @igor.drozdov @nick.thomas @patrickbajao`
1. Add a populated yml file in Gamma
    1. .gitlab-ci.yml should already be there and filled out
1. Unprotect the 2nd master protected branch in Alpha & Gamma and the master protected branch in Beta
1. Change protected branch in Alpha for Allowed to push by setting to Developers + Maintainers
1. Group X default branch protections set to Not Protected
    1. `Group X > Settings > Permissions, LFS, 2FA / Default branch protection`
1. Set project visibility 
    1. Alpha to public (General > Settings > Visibility) 
    1. Beta to internal (should be done already)
1. Modify users to groups and project to create dummy data for audit events
    1. Group X
        1. Add Aubrey Thiel as maintainer
        1. Add Emil Hudson as developer, then change to guest
        1. Remove Lawerence Wisoky, remove direct member and unassign
        1. Alpha
            1. Add Daryl Lindgran as maintainer, change to developer
        1. Beta
            1. Remove Ethelyn Tremblay and unassign
1. Turn off incremental webpack compliation
    1. gitpod /workspace/gitlab-development-kit (main) $ open gdk.yml
    1. set webpack.incremental: false
    1. gdk reconfigure

</details>
