### Monday
---
- [x] Agenda items
- [x] Update GDK 
- [x] Address to-do's
- [x] Sync with Matt
- [ ] ~~Go through `P3` issues for `Next up`~~

### Tuesday
---
- [ ] ~~Setup new desk~~
- [x] Schedule coffee ☕️ chats
- [x]  Usability Session prep
  - [x] Watch recordings from Mike's sessions
  - [x] Watch the first half of the scenarios
- [x] Setup dev credentials
- [x] Iteration discussion notes


### Wednesday
---
- [x] Usability Research for Issue Tracking
- [x] [Merge Request](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37014#note_383900144)
  - WOW that was way more work than I expected
  - [x] Add issue to update badges for `pending removal` on projects

### Thursday
---
- [x] Catch up on to-do's and discussions
- [x] ☕️ chats x3
- [x] Prep and run Issue Worker Usability Session
- [x] Setup up a board for issues or build filters on existing boards
  - [Manage: Compliance UX](https://gitlab.com/groups/gitlab-org/-/boards/1576904?label_name[]=UX&label_name[]=group%3A%3Acompliance)
- [x] Better understand when to use `~UX` `~bug` or `~UX Debt`
- [x] Ad-hoc design collab session with Becka 


### Friday
---
- [x] [Mockup flow for API Approval Rule addition](https://www.figma.com/file/wWTJwed5c4EtRF4oy7Es4f/219567-Create-API-based-approval-rules-for-merge-request-compliance-checks?node-id=75%3A5)
  - [x] [Post walkthrough on unfiltered](https://youtu.be/MzzTDqTykQo)  
- [x] Unfiltered videos 👀
- [x] Usability Session for Issue Worker
- [x] [JTBD for Compliance async catch-up](https://docs.google.com/document/d/1YFQEtWOwHvgcAZj463DJxjPIxQIdc_rr8VpF1uLhqJU/edit#heading=h.hnf7srpuetdd)
- [x] Schedule more ☕️ chats from [Compliance](https://about.gitlab.com/handbook/product/product-categories/#compliance-group)


### Backlog
---
- [ ] Read through [&1385](https://gitlab.com/groups/gitlab-org/-/epics/1385) to better understand GitLab's UX History
- [ ] Read more about the [360 feedback process](https://about.gitlab.com/handbook/people-group/360-feedback/)
- [ ] Create career development plan
  - [ ] Read [handbook page on career development]
  - [ ] Fill out [Mural board for career growth](https://app.mural.co/t/gitlab2474/m/gitlab2474/1594045008555/7fed9cd40e226dda501db2c433bf364821998325)
  - [ ] Discuss with Mike
  - [ ] Check off in Bamboo History
- [ ] Read [Market Guide for Compliance Automation Tools in DevOps](https://drive.google.com/file/d/1Gt-zKKZVdaE62Hu_35HKy9uLXE2QKhvn/view)
- [ ] Look for ways to show context to the team when it comes to the user journey
- [ ] Draft presentation ideas for UX Showcase on Aug 5, 2020
- [ ] Migrate another button component
- [ ] Open an issue to propose a change to the date picker component

