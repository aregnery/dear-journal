### WEEK 35 (2020), MILESTONE [13.4](https://gitlab.com/gitlab-org/gitlab/-/analytics/issues_analytics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acompliance&milestone_title=13.4&assignee_username=aregnery) – W2

- [x] Document example for [Iterative Design](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8373)
- [x] `P1` issues in `workflow::design`
- [x] List goals for issues I am assigned to for `13.4` and mark a health status
- [x] Fix broken tests for [!38939](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38939)
- [x] GitLab Commit 🦊🦝 (Compliance Track)
- [x] Browse Certify: Requirements Management
- [x] Put experiments into motion for issues in `workflow::solution validation`
- [x] Create JTBD Interview Script
- [x] Brain dump ideas for Compliance Dashboard Vision

~"Unplanned Tasks"
- [x] Add design weight to workflow::design

~"To-Do::Next Week"
- [ ] Fix broken tests [!38966](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38966)

~Backlog

- [ ] Identify 1st time experience experiments for [#1192](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1192) | &3839 (Approval Page)
- [ ] 📺 `Create` – Using GitLab to manage Compliance Documentation











