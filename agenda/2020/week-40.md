### WEEK 40 (2020), MILESTONE 13.5 – W3
---
_I am going to be dogfood the iterations feature, so instead of documenting tasks that I want to accomplish outside of GitLab I am going to open an issue and assign it an iteration._

#### 🔁  Issues Tracker
These are tasks that take longer to-do and I try to plan ahead for, and I relate them to issues where most work is being done. I give each issue an assigned iteration (two-week block) and a due date within that. [Feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/221284)
- [Iteration 9](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=aregnery&label_name[]=Iteration%3A%3A9) Oct 1 - 14
- [Iteration 10](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=aregnery&label_name[]=Iteration%3A%3A10) Oct 15 - 28
- [Iteration 11](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=aregnery&label_name[]=Iteration%3A%3A11) Oct 29 - Nov 11

#### ⏰  How did I spend my time this week?
<!-- I'll share my Toggle report at the end of the week -->
![week 40](photos/w_40.jpeg)


<!-- 
```
/assign @aregnery
/label ~UX ~"Iteration::9" ~"group::compliance"
/due October 2
/iteration ?
/weight 3
``` 
-->








