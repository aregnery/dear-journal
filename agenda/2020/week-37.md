### WEEK 37 (2020), MILESTONE [13.4](https://gitlab.com/gitlab-org/gitlab/-/analytics/issues_analytics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acompliance&assignee_username=aregnery) – W5

---
_I am going to be dogfood the iterations feature, so instead of documenting tasks that I want to accomplish outside of GitLab I am going to open an issue and assign it an iteration._

#### 🔁  [Iteration issues](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=aregnery&label_name[]=Iteration%3A%3A8)
These are tasks that take longer to-do and I try to plan ahead for, and I relate them to issues where most work is being done. I give each issue an assigned iteration (two-week block) and a due date within that.

#### 📊  [Issue Analytics](https://gitlab.com/gitlab-org/gitlab/-/analytics/issues_analytics?scope=all&utf8=%E2%9C%93&assignee_username=aregnery)
Trying to also dogfood analytics. Still trying to find a sweet spot for filtering what matters.

#### ⏰  How did I spend my time this week?
<!-- I'll share my Toggle report at the end of the week -->

![week 37](photos/w_37.jpeg)






