## WEEK 34 (2022), MILESTONE 15.4

### ⭐️ Goal: Have a prototype that has been vetted by multiple designers

**Monday**

- [x] Open issue for breadcrumb bug
  - [x] Try to figure out if it is a simple fix
- [x] Update Mac OS
- [x] Observabiltiy investigation
  - [x] Try to turn on feature flag
  - [x] Try to do quick start guide for Opstrace
  - [x] Watch get started with Opstrace
  - [x] Record feedback video
- [x] Review slide deck on north star concepts
- [x] Comments for navigation sync
- [x] Test deleting a forked project
 
**Tuesday**

- [x] Make revisions from yesterday and connect prototype
- [x] Sync with Anne and team
- [x] Collab with Nick
- [x] Stitch together some more interactions
- [x] Record video and schedule time with Jeremy
- [x] Respond to Observability questions

**Wednesday**

- [x] Dig through some more GitLab Observability UI issues for nuggets of knowledge
- [x] Sync with Jeremy
- [x] Weekly navigation sync
- [x] Sync with Nick

**Thursday**

- [x] More prototyping and input from Jeremy session
- [x] Observability sync

**Friday**

- [x] Update gdk
- [x] Observability merge request review
- [x] Catch up on To-Do's
  - [x] Hide features merge request
  - [x] Documentation review on sidebar behavior
  - [x] Incorporate Pedro's suggestion 
  - [x] Respond to Christen's DRI question
