## WEEK 36 (2022), MILESTONE 15.4

### ⭐️ Goal: Finish prototyping and launch studies

**Monday**

- [x] 🇺🇸 Holiday

**Tuesday**

- [x] Sync with Marcel
- [x] Add a few revisions to prototype 1
- [x] Work through vertical navigation on solution 2
- [x] Catch up on to-do's
- [x] Put gdk back before I forget
- [x] Watch the earnings release
- [x] Try designing the siderbar sub menu item

**Wednesday**
- [x] Coffee chat with Paul
- [x] Agenda population
- [x] Merge request review release visbility toggle
- [x] Push forward merge requests
- [ ] Nav sync
- [ ] Skip level
- [ ] Update coverage issue
