## WEEK 32 (2022), MILESTONE 15.3

### ⭐️ Goal: Review nav concept testing and get cracking on the next steps

**Monday**

- [x] UX review of _big_ community contribution
- [x] Office hours
- [x] Watch 20 concept testing videos
- [x] Agenda population

**Tuesday**

- [x] Kick off reviews for tooltip change
- [x] Merge request feedback on direction page
- [x] UX review the Kubernetes change
- [x] Watch the last 2 concept tests
- [x] Start reviewing sidebar speed change

**Wednesday**

- [x] Review the sidebar speed change
- [x] Kubernetes text suggestion
- [x] Mention replies
- [x] Primary vs tertiary in work items
- [x] Doctor appointment (half day)
- [x] Re-review speed of sidebar transition
- [x] Re-review Kubernetes settings

**Thursday**

- [x] Re-review Kubernetes settings
- [x] Watch the recording of the direction page evaluation with PMs
- [x] Watch navigation weekly
- [x] 3 sync meetings
- [x] Fix the help link

**Friday**

- [x] Next badge fix
- [x] Start UX review
- [x] Refine roadmap issue
