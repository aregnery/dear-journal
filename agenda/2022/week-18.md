## WEEK 18 (2022), MILESTONE 15.0

### ⭐️ Goal: Spend more time heads down on navigation designs

**Monday**
- [x] Sync with Marcel on Navigation
- [x] Heads down design time in Figma
- [x] Respond to to-do's

**Tuesday**
- [x] To-Do List → `1`
- [x] Slack questions
- [x] More design time for Nav
- [x] UX Research Responses

**Wednesday**
- [x] More nav design refinement
- [x] Pair with engineering on filtered search
- [x] Sync meetings and agenda prep
- [x] Mention replies

**Thursday**
- [x] Maintainer training reviews x2
- [x] Start milestone planning for 15.1

**Friday**
- [x] Quick action code change

#### ♻️ Thoughts & Feelings

Really busy week just fitting in life, and trying to wrap up loose ends in 15.0.

#### 🗄 Backlog
- [ ] Have a plan in place for 15.1 to cover next week
- [ ] Drafts/discussion feature proposal
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
- [ ] Incorporate quick actions text feedback
- [ ] Next item on card sort issue
- [ ] Quick action menus issue
