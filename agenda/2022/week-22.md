## WEEK 22 (2022), MILESTONE 15.1

### ⭐️ Goal: Get 🍔 menu ideas into testing

**Monday**

- [x] 🇺🇸 Memorial Day

**Tuesday**

- [x] UX Review
- [x] Agendas
- [x] Catchup on outstanding To-Do's
- [x] Sync sessions
- [x] Update prototype

**Wednesday**

- [x] Mege request reviews
- [x] Design 🍐
- [x] 🍔 menu sync with Sid
- [x] Agenda building

**Thursday**

- [x] Design sessions with Sid

**Friday**

- [x] To-Do Catchup
- [x] Merge request reviews
- [x] Planning items for Navigation

#### ♻️ Thoughts & Feelings

Didn't get quite as far with the testing plan as I wanted to. Gotta jump on that next week.

#### 🗄 Backlog

<details><summary>Ideas for another day</summary>

- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic

</details>
