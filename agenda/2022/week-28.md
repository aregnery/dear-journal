## WEEK 28 (2022), MILESTONE 15.2

### ⭐️ Goal: Be ready for 15.3

**Monday**

- [x] Family & Friends Day 🌞

**Tuesday**

- [x] Finalize pitches
- [x] UX reviews on merge requests in 15.2
- [x] Catch up on outstanding to-do list itmes
- [x] Coffee chat
- [x] 15.3 Planning prep
- [x] 2 sync meetings

**Wednesday**

- [x] Upload to Unfiltered
- [x] Wireframe designs
- [x] 2 sync meetings
- [x] Mention replies

**Thursday**

- [x] Working through extracting details

**Friday**

- [x] Vacation 🌴
