## WEEK 15 (2022), MILESTONE 14.10 → 15.0

### ⭐️ Goal: Wrap up UX research planning for card sorting

**Monday**
- [x] Family & Friends Day 🌞

**Tuesday**
- [x] To-do responses
- [x] Review merge requests from Pajamas Migration Day
- [x] eCommerce AMA
- [x] Attempt to fix failing pipelines for Shopify
- [x] Take notes on user research session
- [x] Review diary entry logs

**Wednesday**
- [x] Look back on previous tree testing effort
- [x] Sync meetings x5
- [x] Take notes on user research sessions
- [x] Troubleshoot Gitpod 

**Thursday**
- [x] Look into sign up message on gdk
- [x] Watch a few tutorials on [axe DevTools](https://axe.deque.com/)
- [x] Coffee chat and sync meeting
- [x] Todo replies
- [x] Check box review
- [x] Take notes on user research sessions 
- [x] Open merge request for <kbd>Keyboard</kbd>
- [x] Tweak the proposal for quick actions
- [x] Babysit open merge requests
- [x] Backlog grooming a few issues
- [x] Fix pipelines and resolve conflicts for Shopify

**Friday**
- [x] Quick action MR
- [x] Todo ~~zero~~ `3`
- [x] Take notes on user research sessions
- [x] Move icon merge request
- [x] Check in on Compliance merge requests
- [x] Share learnings on card sorting
- [x] Keyboard shortcut issue


♻️ Thoughts & Feelings

It's amazing how much of a time suck tending to merege requests can be, so I need to keep this number down. I've observed many users hunt for what they are looking for by scrolling through the grouping in navigation, and the more there are the longer it takes. However, they also seem to have their 1-2 pages they visit most which so far have been issues and merge requests.

🗄 Backlog

- [ ] Pick 3 items for seeking community contributions
- [ ] Finish Shopify stuff
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
