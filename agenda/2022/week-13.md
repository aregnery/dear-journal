## WEEK 13 (2022), MILESTONE 14.10

### ⭐️ Goal: Make a pass at the issues in the validation track

**Monday**
- [x] Friends & Family Day 🌞

**Tuesday**
- [x] Compliance responses
- [x] Figure out how to build `gitlab-ui` locally
- [x] UX Weekly
- [x] 10+ To-do's!
- [x] Populate agendas
- [x] Read through 2 validation track issues
- [x] Open a merge request for logo alignement

**Wednesday**
- [x] Inquiry responses
- [x] Merge request reviews x6
- [x] Coffee chat x2

**Thursday**
- [x] Read some blogs
- [x] 10+ to-do's
- [x] Looking into card sorting options
- [x] Contribute to user research issues for navigation efforts
- [x] Check on some of my merge requests that have grown stale

**Friday**
- [x] Get a few merge requests into reviews
- [x] User research notetaking
- [x] Troubleshoot failing jobs
- [x] SUS feedback comment
- [x] `Inbox 0`

### ♻️ Thoughts & Feelings

Busy with lots of different things. Got to spend a little time on Navigation reading, thinking, and designing.

### 🗄 Backlog

- [ ] Pick 3 items for seeking community contributions
- [ ] Finish Shopify stuff
- [ ] Settings needs some backlog love
- [ ] Check in on alerts improvements
- [ ] Refine Unboxing the Advanced Section epic
