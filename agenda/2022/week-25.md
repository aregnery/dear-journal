## WEEK 25 (2022), MILESTONE 15.2

### ⭐️ Goal: Having one prototype for feedback

**Monday**

- [x] US Holiday 🇺🇸

**Tuesday**

- [x] To Do triage
- [x] Retrospective items
- [x] Merge request review
- [x] Mid year check-in
- [x] Watch and read about UX Research insights
- [x] Agenda population
- [x] Try 3D rendering in Figma
- [x] Play around with a Navigation idea

**Wednesday**

- [x] Merge request review
- [x] 4 sync meetings
- [x] Slide deck updates
- [x] Close out old design issue
- [x] Add a work item to the navigation vision

**Thursday**

- [x] Agenda population
- [x] Finish design idea
- [x] Open a new design issue for Nick
- [x] Coffee chat ☕️

**Friday**

- [x] Family and Friends Day 🌞

#### ♻️ Thoughts & Feelings

Week felt tight, but I was able to squeeze in what was most important to me.

#### 🗄 Backlog

<details><summary>Ideas for another day</summary>

- [ ] Self reflect on personal development plan
- [ ] Pick 3 items for seeking community contributions
- [ ] Settings needs some backlog love
- [ ] Refine Unboxing the Advanced Section epic

</details>
